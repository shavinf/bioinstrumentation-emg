Typical Signal Amplitude: 0-10 mV (2 independant signals)

Typical Signal Frequceny : 0-500 Hz

Useful Frequency as specified by literature: 50 -150 Hz


Sources of interference : 
1.  Mains Noise: Present at 50Hz (also has harmonics at multiples of 50 such as 100Hz, 150Hz) 
2.  Motion artifacts: <10 Hz 

 
 
 Amplifier 
1. Need to amplify difference of the two input mV signals to an outpunt singal in the Voltage range 
2. Need to provide enough power to swtich on the LED outputs (typically 3.3V)
3. Need to Aim for a CMRR > 100 in order to properly get rid of common mode voltages. 

Theoretcal Gain Needed = $`Vout/Vin = 3.3V/(10mV+10mV) = 165 `$

This will serve as starting point and may need adjustment as I simulate, build and and test the cricuit. 
 
 
 
Filter: 
1. High Pass Filter : 10Hz cut off (elminate motion artifacts)
2. Notch Filter : Filter out 50 Hz mains noise (fc at 45Hz and 55Hz) 
3. Low Pass Filter : 150Hz cut off (eliminate useless signals) 


 
```mermaid
graph TB

  subgraph "Stages of the Device"
  Node1[Signal] --> Node2[Amplifier]
  Node2 --> Node3[High Pass Filter]
  Node3 --> Node4[Notch Filter]
  Node4 --> Node5[Low Pass Filter]
  Node5 --> Node6[Output as an LED]
  
end
```

