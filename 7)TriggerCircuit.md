Although the peak detector is capable of creating an envlope around the muscle contractions, the output waveform is not constient during this contraction period and will have varying voltage values as shown in the figure below. 
![Peak_Signal](/Images/Trigger/PeakInput.JPG "PeakSignal")

If this output is connected on to an LED, this would cause the LED to flicker , which is not deisred. Thus a Schmitt-Trigger circuit with a threshold voltage of 1V was simulated on multisim to check if it can be used to provide a constant voltage value to the LED during the contraction period. 
![Trigger_Circuit](/Images/Trigger/Circuit.JPG "Trigger_circuit")

The simulated response of the circuit depicts that the circuit was able to provide the required constant voltage during the period where the input signal was above the 1V threshold voltage 

![Circuit_Output](/Images/Trigger/Response.JPG "Circuit_Output")

The circuit was then constructed on the breadboard and the following 2V sinusoidal test signal was inputted into the circuit 
![Trigger_Test_Intput](/Images/Trigger/Input.JPG "Trigger_Test_Intput")

The output obtained from the Schmitt trigger has been overlaid on top of the test input signal. The image clearly shows that the Schmitt trigger is working as desired.
![Trigger_Test_Output](/Images/Trigger/Output.JPG "Trigger_Test_Output")

Therfore the peak detected signal was passed in as the input and the output waveform shown below was obtained from the trigger.
![Trigger_Actual_Output](/Images/Trigger/TriggerOutput.JPG "Trigger_Actual_Output")

The below figure shows the output waveform overlaid on top of the input peak detector signal and it can be clearly observed that the trigger circut only switches on above the threshold voltage, and stays on for the entire duration of the muscle contraction 
![Trigger_Actual_Overlaid](/Images/Trigger/Overlaid.JPG "Trigger_Actual_Overlaid")
