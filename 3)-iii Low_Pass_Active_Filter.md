A second order active low pass filter was designed to truncate the frequency of the test signal in order to extract useful information from the EMG signal.  

The filter was designed to be a Second order , Sallen key filter with a cutoff frequency of 240Hz. This cut off frequency was selected based examining the FFt. It showed that there was some useful information arounf 197Hz
The resistor and capcitor value necessary were caluclated a follows
 
 Transfer function of the 2nd order high pass sallen key filter : 
 
 fc = $`\frac{1}{2\Pi\sqrt{R_1R_2C_1C_2}}`$

Two equivalent capacitors and resistor combinations were selected in order to simplify the cut off freqency equation as follows. 

fc = $`\frac{1}{2\Pi{RC}}`$

Therefore the resistors that were selected were $`R =20k\Omega`$ and the capacitors were $`C =33nF`$ 


**Simulations**
The circut was then constructed and simulated on multism and it can be observed from the below figure that the filter performed as intended. 

![circuitdiagram](Images/Filters/170HzLowpass/Circuit.JPG "multisim circuit signal")
![ac_sweep](Images/Filters/170HzLowpass/Phase.JPG "multisim circuit ac sweep")

However the actual circuit that was constructed on the breadboard performed very differently. As seen form the following  plot, it acted as a low pass filter but the cut off frequency was at 155Hz
This may be due to the Q factor of the sallen key filter deviating from the desired 0.707 value to a new Q factor of 0.5 due to the choice of equivalent capacitors and resistors. 
Therfore the Sallen - Key filter has become critically damped, therby changing it's frequency response. 

![bode](Images/Filters/170HzLowpass/Excel.JPG "excel plot")

Although this was not the desired results, it is still an acceptable result. As the original plan was to attenuate the signal beyond 150Hz. 

Furthermore, the filter was able to cut-off high frequency noise signal as desired. 

![Comparison](Images/Filters/170HzLowpass/Comparison.JPG "multisim circuit signal")


