Two first order, passive high pass filters were implemented with a cut off frequency at 55Hz before the signals enter into the differential amplifier. This was to get rid of any mains noise and to correct for any signal drift that may be present. 
A passive filter was chosen to be implemented as the signal will be feeding straight into the amplifier. 

**Calculations:** 

Cut off frequency: 55Hz 
High pass filter transfer function : 
                fc = $`\frac{1}{2 \Pi RC}`$
                
Assuming the use of a 100nF capacitor, 
                
55 = $`\frac{1}{2 \Pi (R*0.0000001)}`$

R was identified to be 30k$`\Omega`$
                
                
**Simulations:** 
The filter was constructed on multisim and an AC Sweep was conducted to check if the filter would perform as intended.
![circuitdiagram](Images/Filters/55Hz Highpass Filter/CircuitDiagram.JPG "multisim circuit signal")
![phasediagram](Images/Filters/55Hz Highpass Filter/MultisimGraph.JPG "multisim ac sweep signal") 

As observed from the simulation, the cut off freqcueny of the designed filter was approximately 55Hz 

**Construction:** 
The filter was constructed on the breadboad and a 1V sinusoidal signal with varying frequencies was inputted into the filter. The following graph of Gain vs frequency graph was then plotted via the newtork analysis feature of waveforms. The -3dB cut off voltage was 54.6 Hz which is approximately equivalant to the desired 55Hz  

![gaindiagram](Images/Filters/55Hz Highpass Filter/Excel.JPG "gain signal")

Following this, the test signal was passed through the filter and the follwoing result was obtained. 

![signalafterfilteringdiagram](Images/Filters/55Hz Highpass Filter/SignalAfter55Highpass.JPG "signal_after_filtering")

It is clearly evident from the above graph that the high pass passive filter was able to correct for the drift of the input signal.