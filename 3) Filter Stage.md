As specified in the design report, the filter charachteristics requried for the processing of EMG signals are as follows: 

1. High Pass filter with a 10Hz cut off : Filters motion artifacts 
2. Low Pass Filter with a 150Hz cut off : Data above 150 Hz do not provide any useful information for analysis 
3. Notch Filter at 50Hz : Remove AC mains noise from the waveform 

However this is a very generalised range, and therefore a Fast Fourier Transform of the provided test signal was performed on Matlab
to detrmine the power spectrum of the signal and check which frequences carry the most infromation.
![FFt](/Images/Filters/matlab_fft.JPG  "input signal")

Therefore it is evident from the above image that the EMG signal provided in Test Data has a bandwidth approximately between 65Hz and 197Hz.Thus the notch filter can be eliminated by increasing the cut off of the high pass filter.
Furthermore there was a significant levels of noise and drift oberved. Thus an additional high pass filter may be required to correct this change.