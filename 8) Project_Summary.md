__Link to the video:__ [https://youtu.be/Y2zHO24vZA4](https://youtu.be/Y2zHO24vZA4)

This file aims to provide an overall summary of the entire project. 
The device overview is provided by the following flowchart. 

![Final Flowchart](/Images/Flow.JPG "Flow_Chart")

The initial test signals that were provided were of noisy EMG recordings in the milivolt range. One of the tests signals are depicted below. 
![Before_filtered](/Images/Filters/Fiinal_Filter_output/55Hz Highpass Filter/B4Filter.JPG "unfiltered signal") It can be clearly observed that the signal does not sit at the zero baseline and contains large levels of signal drift. 

Thefore in order to elminate this drift levels and filter out any signal nosie due to the mains power supply, a passive High pass filter of 55Hz was implamented at each of the signal channels in order to avoid impedence mismatch.
The effect of these is clearly seen in the figure below. The signal is now centered at the baseline and there is a clear reduction in noise levels. 
![Final_filtered](/Images/Filters/Fiinal_Filter_output/55Hz Highpass Filter/A4filter.JPG "filtered signal")

However, the signal is in the milivolt range and must be amplified in order for it to be useful for further processing. Therfore, the two signals were passed through a differential amplifier that removes any data that is common to both signals and amplifies the output. The result of this amplification is shown in the figure below.
![11_gain_waveforms](/Images/Amplifier/Waveforms_TestSignal_11gain.JPG "11gain")

Following this amplification, the signal underwent further processing via the use of Active High Pass and Active Low pass filteration with 40Hz and 170Hz cut offs repsectively. This enabled the further suprresion of low frequency motion artifacts as well as high frequency noise. 
The filtered output is shown in the figure below. The filtered signal in blue has been overlaid with the unfiltered signal in orange and there is visable noise supression.
![Together](/Images/Filters/Fiinal_Filter_output/Together.JPG "Overlay of the filteredn and unfiltered signals")

Due to the EMG signal containing both positive and negative voltages, the signal can average out to zero. As such a full wave rectifier was used in order to get the absolute value of the EMG signal.
![After_rectification](/Images/Rectifier/AftRectification.JPG "rectified signal")

The rectified signal was noted to be in the milivolt range, which is not suitable for our applications, therfore the signal underwent amplification via a non inverting op-amp in order to bring the signal back into the voltage range. 
![Amp2_Circuit](/Images/Amp_2/AfterAmplifier.JPG "rectified signal")

Following this, the signal was inputted into a peak detection circuit in order to isolate the muscle onctation peaks. This enabled the creation of a signal envelope containting only the mucle contration peaks as shown in the figure below. 
![Output_signal](/Images/PeakDet/Envelope.JPG "Output Wave")

However this signal envelope containted varying voltage levels and therfore is unsuitable to drive an LED. Thus a schmitt trigger was created with a threshold voltage of approximatley 1V. Thus a High output was provided by the trigger whenever the peak detection signal excceded this threshold voltage as shown in the figure below. 
![Trigger_Actual_Overlaid](/Images/Trigger/Overlaid.JPG "Trigger_Actual_Overlaid")

In conclusion the following figure depicts the Schmitt trigger output overlaid with the amplified EMG signal (form the differential amplifer). It can be clearly seen that the circuit is functioning and is able to detect muscle contractions.
![Final Flowchart](/Images/Final.JPG "Flow_Chart")