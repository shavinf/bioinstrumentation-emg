The rectified output of the signal was observed to be in the mV range and as such a it must be amplified.
Thus a non inverting amplifier was used with the output voltage to be within 1.5-4V range. 

Thus the required gain was calculated via 
             $`Gain = \frac{V_{out}}{V_{in}}`$

Therefore 

 $`Gain = \frac{3}{10x10^{-3}} = 300`$

The gain equation for a non inverting amplifier is as follows 
 
 $`Gain = 1+\frac{R_f}{R_g} = 300`$ 
 
 Where R$`_f`$ and R$`_g`$ are the feedback resistor and gain resistor respectively.
 Fixing R$`_g`$ to be $`1k \Omega`$. We can modify the above gain equation as follows:


  $`300 = 1+\frac{R_f}{1k \Omega} `$ which gives us an R$`_f`$ value of approximately  $`300k \Omega`$

Thus the non inverting amplifier was modeled on multisim in order to check it's functionality. The multisim circuit is shown below. 
  ![Amp2_Circuit](/Images/Amp_2/Circuit.JPG "multisim circuit")

The osciloscope output shown below indicates that the signal was amplified as required
![Amp2_Multi_osci](/Images/Amp_2/Phase.JPG "multisim osciloscope output")

The circuit was built on the breadboard and the following rectified input signal was passed into the non inverting input of the amplifier. 
![Amp2_Circuit](/Images/Amp_2/B4Amplifier.JPG "rectified signal")

The output was measured in waveforms and is shown below. It can be seen that the signal was amplified as desired. However an undesired offset has been introduced that must be investigated further. 
![Amp2_Circuit](/Images/Amp_2/AfterAmplifier.JPG "rectified signal")
