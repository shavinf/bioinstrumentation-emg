# Bioinstrumentation - EMG 

__Link to video: [https://youtu.be/Y2zHO24vZA4](https://youtu.be/Y2zHO24vZA4)__

The commits that have been included provide a clear, step by step overview of the entire project and the issues encountered with the project. 
1) EMG Signal Characteristics.md: provides the basic amplitude and frequency characteristics of the EMG signal as well as the type of filtering and amplification that may be required. It also contains the initial flow chart of the device. 
The following three files outline the differential amplification stage of the project, the various gains that were tested and the performance of the final output waveform. They also outline the issues that were encountered and the solutions that were implemented. 
2) Amplification_Stage.md
2)-i Amplification_Stage.md
2)-ii Amplification_Stage.md


These file block contains information on the filters that were used in the project, including the filter design, simulation, theoretical vs actual performances and the issues encountered in the implementation on the circuit. 
3) Filter Stage.md
3)-i: High Pass filter.md
3)-ii High_Pass_Active_Filter.md
3)-iii Low_Pass_Active_Filter.md
3)-iv Final Filtered.md

4) Rectification.md: This file outlines the need for a rectifier as well as the effect of rectification on the EMG signal 
5) 2ndAmplification_Stage.md: Provides the information regarding the design and implementation of the non-inverting amplifier. 
6) Peak Detector.md:  Contains the design, testing and implementation regarding the peak detector circuit used to isolate muscle contraction peaks. 
7)TriggerCircuit.md: This file contains information on the design of the Schmitt Trigger circuit and it’s performance on the breadboard.
8) Project_Summary.md: Summarizes the entire project and provides an updated flowchart outlining the flow of the entire project. __This file also contains the link to the video__
