The following two 10mV waveforms were inputted into the AD623 op-amp with a Gain of 251

![10mVInputs](/Images/Amplifier/Waveforms_10mV_sinusoid.JPG  "input signal")

The following output waveform was obtained:
It can be observed that the voltage of the output sinudoidal waveform was very close to 5V.
![10mVOutputs](/Images/Amplifier/Waveforms_228gain.JPG  "Output signal")

Following this stage, a 10mv sindusodial wave was superimposed with a 60mV noise signal to create a noisy sinusoidal input in to the op-amp as shown below. 
![whitenoiseIutputs](/Images/Amplifier/Waveforms_10mV_whitenoise.JPG "whitenoise signal")

This resulted in an Output waveform that was mostly clipped due to the Vout being above the 5V saturation. 
![whitenoiseIutputs](/Images/Amplifier/Waveforms_whitenoise_output.JPG "whitenoise signal")

This was further confirmed when the test EMG signals were passed through the differential amplifier with the 251 Gain, a significant level of clipping 
of the output wavefrom was again observed. 

![10mVInputs](/Images/Amplifier/Test_Signal.JPG "Test signal")

__Thefore the gain of the OP-Amp must be reduced such that the output waveform sits around the range of 1-2V in order for it to be anaylsed further.__