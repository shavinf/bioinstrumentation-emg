Following the filtering stage, the output waveform must undergo rectification. This is to ensure that the signal does not average out to zero, due to the EMG signal containing both positive and negative voltages. 
As such due to the baseline of the signal being at zero, a full wave rectifier was used in order to get the absolute value of the EMG signal.  

The following full wave rectifier circuit was constructed on multisim 
![Rectification_Circuit](/Images/Rectifier/Circuit.JPG "rectified signal")

Which yielded the desired rectified output as shown below. 

![After_rectification](/Images/Rectifier/response.JPG "rectified signal")

Following this, the circuit was constructed on Multisim in order to check it's functionality. A 1V sinusoidal waveform was inputted into the rectifier circuit and it's output was measured. This is displayed in the grpah below. 

![Multisim Output](/Images/Rectifier/Multisim_Test.JPG "Multisim_Output")

This was followed by sending in the filtered signal shown below.  

![Before_rectification](/Images/Rectifier/B4Rectification.JPG "unrectified signal")

As observed from the folowing figure, the rectification circuit performed as desired and was able rectify the input signal. However, it was noted that the rectified signal has an amplitude in the milivolt range.
![After_rectification](/Images/Rectifier/AftRectification.JPG "rectified signal")



