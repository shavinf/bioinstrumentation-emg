An additional active high pass filter was designed to further supress the any motion artifact data. 

The filter was designed to be a Second order , Sallen key filter with a cutoff frequency of 40Hz. 
The resistor and capcitor value necessary were caluclated a follows
 
 Transfer function of the 2nd order high pass sallen key filter: 
 
 fc = $`\frac{1}{2\Pi\sqrt{R_1R_2C_1C_2}}`$

Two 100nF capcitors were selected and the resistors were adjusted based on tiral and error. 
Therefore the resistors that were selected were $`R_1 =20k\Omega`$ and $`R_2 =40k\Omega`$ 


**Simulations**
The circut was then constructed and simulated on multism and it can be observed from the below figure that the signal performed as intended. 

![circuitdiagram](Images/Filters/40HzHighpass/Circuit.JPG "multisim circuit signal")
![ac_sweep](Images/Filters/40HzHighpass/Bode.JPG "multisim circuit ac sweep")

**Constructions**
The circut was then constructed on the breadboard and it's theoretical vs actual performance was evaluated via testing with sine waves of known frequencies.
The following Network analysis depicts that the cut off frequency of the constructed filter is within the required range of 40Hz.
![BodePlot](Images/Filters/40HzHighpass/Excel.JPG "BodePlotinExcel")


**Implementation**
The test signal was passed through the active high pass filter after being amplified and the following result was obtained 
The trace in the orange colour shows the signal before being filtered and the trace in blue shows the signal after passing through the active high pass filter. 
![Comparison](Images/Filters/40HzHighpass/Comparison.JPG "multisim circuit signal")

It is clearly evident that further noise supression has occured due to the active filter