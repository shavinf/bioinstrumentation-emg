As evident from previous testing, the initial gain value that was used proved to be too high for the test signal. Therefore the R$`_g`$ resistor value was varied until an output waveform between 1-3V was reached such that the output waveform was not clipped.
The R$`_g`$value corresponding to the desired output voltage was 11, and the Rg value was 10K$`\Omega`$.
The amplified output waveform is shown below. 

![11_gain_waveforms](/Images/Amplifier/Waveforms_TestSignal_11gain.JPG "11gain")


It is clear from the above image that the final gain of 11 was able to provide a sufficiently amplified signal without any clipping. 
