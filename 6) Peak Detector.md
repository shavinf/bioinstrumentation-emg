The rectified input was passed through a peak detector in order to detect the muscle contraction peaks. 

The peak detection circuit was built on multisim as shown below. 

![Peak_Circuit](/Images/PeakDet/Circuit.JPG "PeakCircuit")

As seen in the diagram below the circuit functioned as desired. 

![Phase_Circuit](/Images/PeakDet/Phase.JPG "Phase")

The peak detection circuit was built on the breadboard and a 1V sinusoidal signal was inputted into the  circuit. The measured output is shown in the blue waveform below. 
Therfore it can be concluded that the peak detection circuit has been connected appropriately on the breadboard.
![Test_signal](/Images/PeakDet/TestWave.JPG "Test Wave")

The rectified EMG signal  shown in the digram below was fed into the the peak detection circuit.
![Input_signal](/Images/PeakDet/Input.JPG "Input Wave")

The following output waveform was obtianed. It clearly shows the signal envelope for the muscle contraction peaks and that the peak detection circuit functions as desired.
![Output_signal](/Images/PeakDet/Envelope.JPG "Output Wave")
