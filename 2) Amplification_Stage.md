This stage is designed to amplify the incoming mV signal to a voltage signal that can be used.
The AD623 amplifier that was provided in our kit was used as there was an inbuilt multisim model of the Differential Amplifier.
This would allow me to compare the simulated outputs with the actual circuit outputs and perform troubleshooting as required.

The gain equation for the AD623 was obtained via the [Datasheet][Datasheet].

 
Gain = $` 1+\frac{100k}{R_g}`$

As previously mentioned in Document 1, my starting gain for the simulation was approximately 165. This required an R$`_g`$ resistor of 600$`\Omega`$ which resulted in gain of 168 in the op amp.

The Amplifier was modeled on multisim with two 10mV sinuodial signals that are out of phase with each other as shown below. 

![167gain_multisim_circuit](/Images/Amplifier/167gain.JPG "167gain")

This yielded an output as shown in the figure below 

![167gain_multisim_outout](/Images/Amplifier/167gain_simulation.JPG "167gain_output")

This shows that a gain of 168 is too small as does not provide us with an output high enough to switch on an LED. 

Therfore the gain was increased to 251 by placing an R$`_g`$ resistor of 400$`\Omega`$ which resulted in an output waveform as shown below. 

![251gain_multisim_outout](/Images/Amplifier/251gain_simulation.JPG "251gain_output")

This shows that a Gain of 251 yeilds a signal that is consistently above the desired 3.3 V 

[Datasheet]:<https://www.analog.com/media/en/technical-documentation/data-sheets/AD623.pdf>