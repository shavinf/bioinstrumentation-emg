This document provides a clear summary of the effects caused by filtering of the EMG signal. 

The figure below depicts one of the input channels contianing one of the EMG signals. It can be observed that the signal is very noisy and there is a clear presence of drift within the signal.  
![Before_filtered](/Images/Filters/Fiinal_Filter_output/55Hz Highpass Filter/B4Filter.JPG "unfiltered signal")


Following the passive highpass filtering, this drift has been corrected and there is an observable reduction in noise levels. 
![Final_filtered](/Images/Filters/Fiinal_Filter_output/55Hz Highpass Filter/A4filter.JPG "filtered signal")

Furthermore the figure below illustrates the wavform following amplification via the AD623 differential amplifier. Noticable levels of noise can still be observed within the signal and therfore the signal was passed through active highpass and low pass filter stages in order to truncate the signal to the requried bandwidth and filter any low frequency motion artifacts of high frequency noise. 
![Before_filtered](/Images/Filters/Fiinal_Filter_output/B4Filter.JPG "unfiltered signal")

Following the filtering stages, the resultant output was as shown below. There is a clear reduction of the noise levels within the filtered ouput and the EMG peaks are more distinguishable within the filtered signal. 
![Final_filtered](/Images/Filters/Fiinal_Filter_output/A4Filter.JPG "filtered signal")

The figure below shows the signals overlaid on top of each other, in order to see the differences more clearly. 
![Together](/Images/Filters/Fiinal_Filter_output/Together.JPG "Overlay of the filteredn and unfiltered signals")